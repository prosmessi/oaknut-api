import mongoose, { Document, Model } from 'mongoose';

const Schema = mongoose.Schema;

const userSchema = new Schema(
  {
    fullName: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
    password: {
      type: String,
    },
    authProvider: {
      type: String,
    },
    socketId: String,
    image: String,
    activity: {
      status: String,
      lastOnline: Date,
    },
  },
  { timestamps: true },
);

export interface IUser {
  fullName: string;
  email: string;
  authProvider: 'GOOGLE' | 'FACEBOOK';
  password?: string;
  image: string;
  activity: {
    status: string;
    lastOnline: Date;
  };
  socketId: string;
}

export interface IUserDocument extends IUser, Document {}

export interface IUserModel extends Model<IUserDocument> {}

export default mongoose.model<IUserDocument, IUserModel>('User', userSchema);
