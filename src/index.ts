import express, { NextFunction, Request, Response } from 'express';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import swaggerJsDoc from 'swagger-jsdoc';
import swaggerUi, { SwaggerOptions } from 'swagger-ui-express';

// Routes
import authRoutes from './routes/userRoutes';

const app = express();
const port = process.env.PORT || 5200;

require('dotenv').config();

// MongoDB URI | Special
const uri = `mongodb+srv://${process.env.MONGODB_USERNAME}:${process.env.MONGODB_PASSWORD}cluster0-zlxgj.mongodb.net/${process.env.MONGODB_DB_NAME}`;
// const uri = `mongodb://localhost:27017/${process.env.MONGODB_DB_NAME}`;

const swaggerOptions: SwaggerOptions = {
  swaggerDefinition: {
    info: {
      title: 'OakNut Api',
      description: 'A social media platform',
      contact: {
        name: 'DNDeveloper',
      },
      servers: [
        process.env.NODE_ENV
          ? 'https://oaknut-api.herokuapp.com/'
          : process.env.SERVER_URL + ':' + port,
      ],
    },
  },
  apis: [process.env.SWAGGER_ROUTES_URL],
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs));

// Setting up api access permissions
app.use(
  (
    req: any,
    res: { setHeader: (arg0: string, arg1: string) => void },
    next: () => void,
  ) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader(
      'Access-Control-Allow-Methods',
      'GET, POST, PUT, PATH, DELETE',
    );
    res.setHeader(
      'Access-Control-Allow-Headers',
      'Content-Type, Authorization',
    );

    next();
  },
);

// Setting up json parser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Serving static files with express
app.use(express.static('public'));

// Setting up routes with some default

app.use(authRoutes);
// app.use('/api', apiRoutes);
// app.use('/label', labelRoutes);
// app.use('/project', projectRoutes);
// app.use('/task', taskRoutes);
// app.use(swaggerRoutes);

// Setting up special error middleware
app.use(
  (
    err: { status: number; message: string; errorKey?: string },
    req: Request,
    res: Response,
    next: NextFunction,
  ) => {
    res.status(err.status || 500);
    res.send({
      ok: false,
      status: err.status || 500,
      message: err.message,
      errorKey: err.errorKey,
    });
  },
);

// Setting up connection to MongoDB
mongoose.connect(uri).then(() => {
  const server = app.listen(port);

  // Setting up connection to Socket.io
  // const io = require('./socket.ts')(server);
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  // const io = require('./socket')(server);
  //
  // app.set('socket.io', io);

  // eslint-disable-next-line no-console
  console.log('Sever listening to ' + process.env.PORT || 5000);
});
