import { NextFunction, Request, Response } from 'express';
import { JsonObject } from 'swagger-ui-express';
import bcrypt from 'bcryptjs';
import createError from 'http-errors';
import { validationResult } from 'express-validator';
import jwt from 'jsonwebtoken';
import { OAuth2Client } from 'google-auth-library';
import fetch from 'node-fetch';

import { sendResponseInJson } from '../helpers/utils';
import User, { IUserDocument } from '../models/User';
import sendMail from '../helpers/sendMail';
import { getResetPasswordHtml } from '../helpers/parseHtml';

interface ILoginReturn {
  expiresIn: string;
  token: string;
}

function logInUser(user: IUserDocument): ILoginReturn {
  const expiresIn = '2h';
  const token = jwt.sign(
    {
      userId: user._id.toString(),
      email: user.email,
    }, // Data passed with the token
    process.env.JWT_SECRET_KEY as string, // Secret key
    { expiresIn }, // Expiration time
  );

  return { expiresIn, token };
}

/**
 *
 * @swagger
 * tags:
 *   name: User
 */

/**
 * @swagger
 * definitions:
 *   User:
 *     type: object
 *     properties:
 *       fullName:
 *         type: string
 *         example: Saurabh Singh
 *       email:
 *         type: string
 *         example: example@gmail.com
 *       password:
 *         type: string
 *         example: strong password
 *       image:
 *         type: string
 *         description: not necessary, it can be taken through social media if that was the case
 *         example: path to the image
 *       socketId:
 *         type: string
 *         example: 25bhsdsa1127dha
 *   ShortUserObjResponse:
 *     type: object
 *     properties:
 *       _id:
 *         type: string
 *         example: 5fe344152d0fea20443f687c
 *       fullName:
 *         type: string
 *         example: Saurabh Singh
 *       email:
 *         type: string
 *         example: example@gmail.com
 *       image:
 *         type: string
 *         example: path to the image
 *   ApiResponse:
 *     type: object
 *     properties:
 *       ok:
 *         type: boolean
 *         description: If the response is ok or not
 *       message:
 *         type: string
 *         description: Response message
 *       data:
 *         type: object
 *         description: Object with some data
 *   ErrorObj:
 *     type: object
 *     properties:
 *       ok:
 *         type: boolean
 *         description: If the response is ok or not
 *       status:
 *         type: integer
 *         description: Status Code
 *       message:
 *         type: string
 *         description: Response message
 *       errorKey:
 *         type: string
 *         description: It actually refer the key for the form input
 */

/**
 *
 * @swagger
 * /signup:
 *   post:
 *     tags: [User]
 *     summary: Sign up the new user
 *     description: ""
 *     consumes:
 *     - application/json
 *     produces:
 *     - application/json
 *     parameters:
 *     - in: body
 *       name: body
 *       description: Request Body that contains the field
 *       required:
 *       schema:
 *         required:
 *           - fullName
 *           - email
 *           - password
 *           - con_password
 *         type: object
 *         properties:
 *           fullName:
 *             type: string
 *             example: Saurabh Singh
 *           email:
 *             type: string
 *             example: example@gmail.com
 *           password:
 *             type: string
 *             example: CapitalNumberWithSpecialChars
 *           con_password:
 *             type: string
 *             example: CapitalNumberWithSpecialChars
 *           imageUrl:
 *             type: string
 *           socketId:
 *             type: string
 *     responses:
 *       200:
 *         description: User Created successfully
 *         schema:
 *           $ref: '#/definitions/ApiResponse'
 */

export const signUpController = async (
  req: Request,
  res: Response,
  next: NextFunction,
): Promise<JsonObject | void> => {
  try {
    const { fullName, email, imageUrl, password, con_password, socketId } = req.body;

    // Here, Handling the validation result from the express-validator
    const results = validationResult(req);
    if (!results.isEmpty()) {
      const firstError = results.array({ onlyFirstError: true })[0];
      return next(
        createError(400, firstError.msg, {
          errorKey: firstError.param,
        }),
      );
    }

    // Checking if the user already exists with the email
    // If exists then sending the response with the existing express error handling middleware
    // to throw to the client
    let user = await User.findOne({ email });
    if (user) {
      return next(
        createError(409, 'User with the email already exists', {
          errorKey: 'email',
        }),
      );
    }

    // Hashing the password
    const salt = bcrypt.genSaltSync(10);
    const hashedPassword = bcrypt.hashSync(password, salt);

    // TODO: Storing the image and getting the link

    // Adding user to the mongodb
    user = new User({
      fullName,
      email,
      password: hashedPassword,
      image: imageUrl || '/assets/images/def_user.png',
      socketId,
    });

    // Saving the database
    await user.save();

    return sendResponseInJson(
      {
        ok: true,
        message: 'User has been registered successfully!',
        data: {
          userId: user._id,
          fullName: user.fullName,
          email: user.email,
          image: user.image,
        },
      },
      res,
    );
  } catch (error) {
    return next(createError(500, error.message, { errorKey: 'serverErr' }));
  }
};

/**
 *
 * @swagger
 * /login:
 *   post:
 *     tags: [User]
 *     summary: Login the new user
 *     description: ""
 *     consumes:
 *     - application/json
 *     produces:
 *     - application/json
 *     parameters:
 *     - in: body
 *       name: body
 *       description: Request Body that contains the field
 *       required:
 *         - email
 *         - password
 *       schema:
 *         type: object
 *         properties:
 *           email:
 *             type: string
 *             example: example@gmail.com
 *           password:
 *             type: string
 *             example: CapitalNumberWithSpecialChars
 *     responses:
 *       200:
 *         description: Logged in successfully
 *         schema:
 *           type: object
 *           properties:
 *             ok:
 *               type: boolean
 *               description: If the response is ok or not
 *             message:
 *               type: string
 *               description: Message with response
 *             data:
 *               type: object
 *               properties:
 *                 token:
 *                   type: string
 *                   description: JSON login token,
 *                 expiresIn:
 *                   type: string
 *                   description: Details about the token expiration in 'CountHour' string such as '1h', '2h'
 */
export const loginController = async (
  req: Request,
  res: Response,
  next: NextFunction,
): Promise<JsonObject | void> => {
  try {
    const { email, password } = req.body;

    // Here, Handling the validation result from the express-validator
    const results = validationResult(req);
    if (!results.isEmpty()) {
      const firstError = results.array({ onlyFirstError: true })[0];
      return next(
        createError(400, firstError.msg, {
          errorKey: firstError.param,
        }),
      );
    }

    // Checking if the user with the email exists or not
    const user = await User.findOne({ email });
    if (!user) {
      return next(
        createError(401, 'User with the email not found!', {
          errorKey: 'email',
        }),
      );
    }

    // Checkin if the user is from auth for what
    if (!user.password || user.authProvider) {
      return next(
        createError(
          401,
          'Try signing with the correct auth provider, you are not signed in with us.',
          { errorKey: 'password' },
        ),
      );
    }

    // Matching the encrypted password
    const doesMatch = bcrypt.compareSync(password, user.password);
    if (!doesMatch) {
      return next(
        createError(401, `Password doesn't match`, { errorKey: 'password' }),
      );
    }

    // Generating the token using the jwt(JSON WEB TOKEN) Service and signing with some metadata
    const { expiresIn, token } = logInUser(user);

    return sendResponseInJson(
      {
        ok: true,
        message: 'Logged in successfully!',
        data: {
          token,
          expiresIn,
        },
      },
      res,
    );
  } catch (error) {
    return next(createError(500, error.message, { errorKey: 'serverErr' }));
  }
};

/**
 *
 * @swagger
 * /reset-password:
 *   post:
 *     tags: [User]
 *     summary: Resetting the password for the existing user
 *     description: ""
 *     consumes:
 *     - application/json
 *     produces:
 *     - application/json
 *     parameters:
 *     - in: body
 *       name: body
 *       description: Request Body that contains the field
 *       required:
 *         - email
 *       schema:
 *         type: object
 *         properties:
 *           email:
 *             type: string
 *             example: example@gmail.com
 *     responses:
 *       200:
 *         description: Mail sent successfully, Follow the instructions there!
 *         schema:
 *           $ref: '#/definitions/ApiResponse'
 */
export const resetPasswordController = async (
  req: Request,
  res: Response,
  next: NextFunction,
): Promise<JsonObject | void> => {
  try {
    const { email } = req.body;

    // Here, Handling the validation result from the express-validator
    const results = validationResult(req);
    if (!results.isEmpty()) {
      const firstError = results.array({ onlyFirstError: true })[0];
      return next(
        createError(400, firstError.msg, {
          errorKey: firstError.param,
        }),
      );
    }

    // Check if the acc exists with the email
    const user = await User.findOne({ email });
    if (!user) {
      return next(
        createError(
          406,
          'No account associated with the email, Try to Signup.',
          {
            errorKey: 'email',
          },
        ),
      );
    }

    // Check for the auth provider of the user
    if (user.authProvider || !user.password) {
      return next(
        createError(
          406,
          'This account is registered with connected provider.',
          {
            errorKey: 'email',
          },
        ),
      );
    }

    // Generating the jwt token for resetting the password
    const expiresIn = '2h';
    const token = jwt.sign(
      {
        userId: user._id.toString(),
        email: user.email,
        passwordChange: true,
      }, // Data passed with the token
      process.env.JWT_SECRET_KEY as string, // Secret key
      { expiresIn }, // Expiration time
    );

    // Actually parsing the html data to send via email
    const parsedHTML = getResetPasswordHtml(
      `http://oaknut.ml/?showModal=true&auth=true&curAuth=change-password&token=${token}`,
    );

    // Finally Sending the mail using nodemailer
    sendMail({
      to: email || 'saurs2000@gmail.com',
      subject: 'Reset OakNut Password',
      html: parsedHTML,
    });

    return sendResponseInJson(
      {
        ok: true,
        message: 'Mail sent successfully, Follow the instructions there!',
        data: {},
      },
      res,
    );
  } catch (error) {
    return next(createError(500, error.message, { errorKey: 'serverErr' }));
  }
};

/**
 *
 * @swagger
 * /validate-token:
 *   post:
 *     tags: [User]
 *     summary: Validating the token
 *     description: ""
 *     consumes:
 *     - application/json
 *     produces:
 *     - application/json
 *     parameters:
 *     - in: body
 *       name: body
 *       description: Body has the token property
 *       required:
 *         - token
 *       schema:
 *         type: object
 *         properties:
 *           token:
 *             type: string
 *     responses:
 *       200:
 *         description: Token validated successfully!
 *         schema:
 *           $ref: '#/definitions/ApiResponse'
 *       404:
 *         description: No user found with this user id
 *         schema:
 *           $ref: '#/definitions/ErrorObj'
 */
export const validateTokenController = async (
  req: Request,
  res: Response,
  next: NextFunction,
): Promise<JsonObject | void> => {
  try {
    const { token } = req.body;

    // Verifying the JWT Token
    const decodedToken: any = jwt.verify(
      token,
      process.env.JWT_SECRET_KEY as string,
    );

    // If token is expired or not valid
    if (!decodedToken) {
      return next(createError(401, 'Token is expired or not valid.'));
    }

    // If token is actually for the password change and not for the login or any other service
    if (!decodedToken.passwordChange) {
      return next(createError(401, 'This token is not for this task!'));
    }

    // Sending the response if the token is all set and valid
    return sendResponseInJson(
      {
        ok: true,
        message: 'Token validated successfully!',
        data: {},
      },
      res,
    );
  } catch (error) {
    return next(createError(500, error.message, { errorKey: 'serverErr' }));
  }
};

/**
 *
 * @swagger
 * /change-password:
 *   post:
 *     tags: [User]
 *     summary: Changing the password
 *     description: ""
 *     consumes:
 *     - application/json
 *     produces:
 *     - application/json
 *     parameters:
 *     - in: body
 *       name: body
 *       description: Body has the token password and con_password
 *       required:
 *         - token
 *       schema:
 *         type: object
 *         properties:
 *           token:
 *             type: string
 *           password:
 *             type: string
 *           con_password:
 *             type: string
 *     responses:
 *       200:
 *         description: Password changed successfully!
 *         schema:
 *           $ref: '#/definitions/ApiResponse'
 *       404:
 *         description: User details not found!
 *         schema:
 *           $ref: '#/definitions/ErrorObj'
 *       401:
 *         description: Not authorized
 *         schema:
 *           $ref: '#/definitions/ErrorObj'
 */
export const changePasswordController = async (
  req: Request,
  res: Response,
  next: NextFunction,
): Promise<JsonObject | void> => {
  try {
    const { password, token } = req.body;

    // Verifying the JWT Token
    const decodedToken: any = jwt.verify(
      token,
      process.env.JWT_SECRET_KEY as string,
      // TODO: Check this option
      // {complete: true}
    );

    // If token is expired or not valid
    if (!decodedToken) {
      return next(createError(401, 'Not authorized!'));
    }

    // Here, Handling the validation result from the express-validator
    const results = validationResult(req);
    if (!results.isEmpty()) {
      const firstError = results.array({ onlyFirstError: true })[0];
      return next(
        createError(400, firstError.msg, {
          errorKey: firstError.param,
        }),
      );
    }

    // Storing the userId which is present in the JWT Payload
    const userId = decodedToken.userId;
    const user = await User.findById(userId);

    // Handling if the user is not present with this id
    if (!user) {
      return next(
        createError(404, 'User details not found!', {
          errorKey: 'email',
        }),
      );
    }

    // Check for the auth provider of the user
    if (user.authProvider || !user.password) {
      return next(
        createError(406, 'This account is registered with connected provider.'),
      );
    }

    // Hashing the password
    const salt = bcrypt.genSaltSync(10);
    const hashedPassword = bcrypt.hashSync(password, salt);

    // Updating the hashed password to the mongodb database
    await User.findByIdAndUpdate(
      userId,
      { password: hashedPassword },
      { omitUndefined: true },
    );

    return sendResponseInJson(
      {
        ok: true,
        message: 'Password changed successfully!',
        data: {},
      },
      res,
    );
  } catch (error) {
    return next(createError(500, error.message, { errorKey: 'serverErr' }));
  }
};

/**
 *
 * @swagger
 * /user:
 *   get:
 *     tags: [User]
 *     summary: Fetching the current user
 *     description: ""
 *     consumes:
 *     - application/json
 *     produces:
 *     - application/json
 *     parameters:
 *     - in: headers
 *       name: headers
 *       description: Request Header contains the Authorization key with the token "Bearer **token goes here**"
 *     responses:
 *       200:
 *         description: User details fetched successfully
 *         schema:
 *           $ref: '#/definitions/ApiResponse'
 *       404:
 *         description: No user found with this user id
 *         schema:
 *           $ref: '#/definitions/ErrorObj'
 */
export const fetchUserController = async (
  req: Request | any,
  res: Response,
  next: NextFunction,
): Promise<JsonObject | void> => {
  try {
    const userId = req.userId;

    // Fetching the user from the mongodb database
    const user = await User.findById(userId);

    // If no user is found in the mongodb database
    if (!user) {
      return next(createError(404, 'No user found with this user id'));
    }

    return sendResponseInJson(
      {
        ok: true,
        message: 'User details fetched successfully',
        data: {
          id: user._id,
          fullName: user.fullName,
          image: user.image,
          email: user.email,
        },
      },
      res,
    );
  } catch (error) {
    return next(createError(500, error.message, { errorKey: 'serverErr' }));
  }
};

/**
 *
 * @swagger
 * /social-login:
 *   post:
 *     tags: [User]
 *     summary: Logging or Signing up the user using social logins Facebook or Google
 *     description: ""
 *     consumes:
 *     - application/json
 *     produces:
 *     - application/json
 *     parameters:
 *     - in: body
 *       name: body
 *       description: Body has the token password and con_password
 *       schema:
 *         type: object
 *         required:
 *           - accessToken
 *           - provider
 *         properties:
 *           provider:
 *             type: string
 *             enum: [GOOGLE, FACEBOOK]
 *           accessToken:
 *             type: string
 *           socketId:
 *             type: string
 *     responses:
 *       200:
 *         description: Logged in successfully
 *         schema:
 *           $ref: '#/definitions/ApiResponse'
 *       401:
 *         description: Either token is not valid or expired
 *         schema:
 *           $ref: '#/definitions/ErrorObj'
 */
export const socialLoginController = async (
  req: Request,
  res: Response,
  next: NextFunction,
): Promise<JsonObject | void> => {
  try {
    // Provider can be one of twos are 'GOOGLE' | 'FACEBOOK';
    const { provider, accessToken, socketId } = req.body;

    let fullName, email, image;

    if (provider !== 'GOOGLE' && provider !== 'FACEBOOK') {
      return next(createError(400, 'Current Provider is not supported yet!'));
    }

    // GOOGLE AUTH
    if (provider === 'GOOGLE') {
      const CLIENT_ID = process.env.GOOGLE_CLIENT_ID;
      const client = new OAuth2Client(CLIENT_ID);
      const ticket = await client.verifyIdToken({
        idToken: accessToken,
        audience: CLIENT_ID, // Specify the CLIENT_ID of the app that accesses the backend
        // Or, if multiple clients access the backend:
        //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
      });

      // Handling the payload getting from the google api
      const payload = ticket.getPayload();
      if (!payload) {
        return next(createError(401, 'Invalid token buddy!'));
      }

      // Handling if the email is not verified
      const {
        email: googleEmail,
        email_verified: emailVerified,
        name,
        picture,
      } = payload;

      if (!emailVerified) {
        return next(createError(401, 'Email is not verified!'));
      }

      email = googleEmail;
      fullName = name;
      image = picture;
    }

    // FACEBOOK AUTH
    if (provider === 'FACEBOOK') {
      // Validating the token that
      // The token is being generated for OakNut App
      const url = `https://graph.facebook.com/debug_token?input_token=${accessToken}&access_token=${process.env.FACEBOOK_ACCESS_TOKEN}`;
      const response = await fetch(url);
      const payload = await response.json();

      // Handling the payload
      if (!payload.data.is_valid) {
        return next(createError(401, 'Invalid token buddy!'));
      }

      // Fetching the userDetails from the facebook using the Facebook Graph API 
      const urlToFetchUserDetails = `https://graph.facebook.com/${payload.data.user_id}/?fields=id,name,email,picture&access_token=${accessToken}`;
      const responseOfUserDetails = await fetch(urlToFetchUserDetails);
      const payloadUserDetails = await responseOfUserDetails.json();

      email = payloadUserDetails.email;
      fullName = payloadUserDetails.name;
      image = payloadUserDetails.picture.data.url;
    }

    // Checking if user already exists in the database
    let user = await User.findOne({ email });

    // Checking if the user exists with the email and if the user is not with this provider, then dont go forward
    if (user && (!user.authProvider || user.authProvider !== provider)) {
      return next(
        createError(
          401,
          'You are not signed up with this provider using this email',
        ),
      );
    }

    // If user is not in the database
    if (!user) {
      user = new User({
        fullName,
        email,
        authProvider: provider,
        image,
        socketId,
      });

      await user.save();
    }

    // Getting the jwt token
    const { expiresIn, token } = logInUser(user);

    // Sending the response
    return sendResponseInJson(
      {
        ok: true,
        message: `Logged in successfully using ${provider}!`,
        data: {
          token,
          expiresIn,
          // TODO: Delete these testing properties just for development purpose
          user: {
            fullName,
            email,
            authProvider: provider,
            image,
            socketId,
          },
        },
      },
      res,
    );
  } catch (error) {
    return next(createError(500, error.message, { errorKey: 'serverErr' }));
  }
};

/**
 * 
 * {
   "id": "2777415879219591",
   "name": "Saurabh Singh",
   "email": "saurs2000\u0040gmail.com",
   "picture": {
      "data": {
         "height": 50,
         "is_silhouette": false,
         "url": "https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=2777415879219591&height=50&width=50&ext=1636109012&hash=AeSIHJcTsLj5PAEvqEw",
         "width": 50
      }
   }
}
 * 
 */

/**
 *
 * {
    "data": {
        "app_id": "188130890104581",
        "type": "USER",
        "application": "OakNut",
        "data_access_expires_at": 1641284935,
        "expires_at": 1633510800,
        "is_valid": true,
        "scopes": [
            "email",
            "public_profile"
        ],
        "user_id": "2777415879219591"
    }
}
 *
 */

/**
 *
 * {
    "message": "Done",
    "payload": {
        "iss": "accounts.google.com",
        "azp": "25568487206-c9e9ennf0h0r626cnkph9d9bi2qmqqte.apps.googleusercontent.com",
        "aud": "25568487206-c9e9ennf0h0r626cnkph9d9bi2qmqqte.apps.googleusercontent.com",
        "sub": "108703767804912967082",
        "email": "saurs2000@gmail.com",
        "email_verified": true,
        "at_hash": "ZdlGtUrIUjcJAt2JmZppBQ",
        "name": "DNDEVELOPER",
        "picture": "https://lh3.googleusercontent.com/a-/AOh14GgSo4xEGDJdaEKpvIyvGPGh07T62rvepOFYBEzQPw=s96-c",
        "given_name": "DNDEVELOPER",
        "locale": "en",
        "iat": 1633505662,
        "exp": 1633509262,
        "jti": "cc3845cf66e370f99c007a2f4cc7fd616b08dd16"
    }
}
 *
 */

/**
 *
 *     parameters:
 *     - in: formData
 *       name: email
 *       description: Form Data that contains the field
 *       required: true
 *       schema:
 *         type: string
 *     - in: formData
 *       name: password
 *       description: Password associated with the account
 *       required: true
 *       schema:
 *         type: string
 */
