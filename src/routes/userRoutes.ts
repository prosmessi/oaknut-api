import express from 'express';
import { body } from 'express-validator';
import {
  changePasswordController,
  fetchUserController,
  loginController,
  resetPasswordController,
  signUpController,
  socialLoginController,
  validateTokenController,
} from '../controllers/userControllers';

import isAuth from '../middlewares/isAuth';

const router = express.Router();

router.post(
  '/signup',
  // Full name must be at least 5 chars long
  body('fullName')
    .escape()
    .trim()
    .isLength({ min: 5 })
    .withMessage('Full Name must be at least 5 characters'),
  // email must be an email
  body('email')
    .trim()
    .escape()
    .isEmail()
    .normalizeEmail()
    .withMessage('Invalid Email'),
  // password must be at least 8 chars long
  body('password')
    .trim()
    .isLength({ min: 8, max: 24 })
    .withMessage('Must have at least 8 digits and at most 24 digits')
    .matches(/\d/)
    .withMessage('Must contain at least a number')
    .matches(/[A-Z]/)
    .withMessage('Must contain at least one Uppercase letter')
    .matches(/[-+_!@#$%^&*.,?]/)
    .withMessage('Must contain one of these [-+_!@#$%^&*.,?]'),
  body('con_password')
    .trim()
    .custom((value: any, { req }: any) => {
      if (value !== req.body.password) {
        throw new Error("Confirm Password doesn't match");
      }
      return true;
    }),
  body('imageUrl')
    .trim()
    .escape(),
  signUpController,
);

router.post(
  '/login',
  body('email')
    .trim()
    .escape()
    .isEmail()
    .normalizeEmail()
    .withMessage('Invalid Email'),
  body('password')
    .trim()
    .isLength({ min: 8, max: 24 })
    .withMessage('Must have at least 8 digits and at most 24 digits')
    .custom((value: any) => {
      if (typeof value !== 'string') {
        throw new Error('Try to reenter password, Its not of type string!');
      }
      return true;
    }),
  loginController,
);

router.post(
  '/reset-password',
  body('email')
    .trim()
    .escape()
    .isEmail()
    .normalizeEmail()
    .withMessage('Invalid Email'),
  resetPasswordController,
);

router.get('/user', isAuth, fetchUserController);

router.post('/validate-token', validateTokenController);

router.post(
  '/change-password',
  // password must be at least 8 chars long
  body('password')
    .trim()
    .isLength({ min: 8, max: 24 })
    .withMessage('Must have at least 8 digits and at most 24 digits')
    .matches(/\d/)
    .withMessage('Must contain at least a number')
    .matches(/[A-Z]/)
    .withMessage('Must contain at least one Uppercase letter')
    .matches(/[-+_!@#$%^&*.,?]/)
    .withMessage('Must contain one of these [-+_!@#$%^&*.,?]'),
  body('con_password')
    .trim()
    .custom((value: any, { req }: any) => {
      if (value !== req.body.password) {
        throw new Error("Confirm Password doesn't match");
      }
      return true;
    }),
  changePasswordController,
);

router.post('/social-login', socialLoginController);

export default router;
