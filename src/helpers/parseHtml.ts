export const getResetPasswordHtml = (resetLink: string): string => {
  return `
<body style='background-color: #fff; margin: 0; padding: 0; -webkit-text-size-adjust: none; text-size-adjust: none;'>
<table border='0' cellpadding='0' cellspacing='0' class='nl-container' role='presentation'
       style='mso-table-lspace: 0; mso-table-rspace: 0; background-color: #fff;' width='100%'>
  <tbody>
  <tr>
    <td>
      <table align='center' border='0' cellpadding='0' cellspacing='0' class='row row-1'
             role='presentation' style='mso-table-lspace: 0; mso-table-rspace: 0;' width='100%'>
        <tbody>
        <tr>
          <td>
            <table align='center' border='0' cellpadding='0' cellspacing='0'
                   class='row-content stack' role='presentation'
                   style='mso-table-lspace: 0; mso-table-rspace: 0;' width='500'>
              <tbody>
              <tr>
                <td
                  style='width:100%;padding-right:0px;padding-left:0px;padding-top:5px;padding-bottom:5px;'>
                  <div align='center'
                       style='line-height:10px;display: flex;align-items: center;justify-content: center; padding: 24px 0; background-color: #e6e6e6;'>

                    <img style='display: block; margin: 0 auto; height: auto; border: 0; width: 140px; max-width: 100%;'
                         src='https://oaknut-api.herokuapp.com/assets/logos/OakNut%20Full%20logo.png' alt=''>
                  </div>
                </td>
              </tr>
              </tbody>
            </table>
          </td>
        </tr>
        </tbody>
      </table>
      <table align='center' border='0' cellpadding='0' cellspacing='0' class='row row-2'
             role='presentation' style='mso-table-lspace: 0; mso-table-rspace: 0;' width='100%'>
        <tbody>
        <tr>
          <td>
            <table align='center' border='0' cellpadding='0' cellspacing='0'
                   class='row-content stack' role='presentation'
                   style='mso-table-lspace: 0; mso-table-rspace: 0;' width='500'>
              <tbody>
              <tr>
                <th class='column'
                    style='mso-table-lspace: 0; mso-table-rspace: 0; font-weight: 400; text-align: left; vertical-align: top;'
                    width='100%'>
                  <table border='0' cellpadding='0' cellspacing='0'
                         class='heading_block' role='presentation'
                         style='mso-table-lspace: 0; mso-table-rspace: 0;' width='100%'>
                    <tr>
                      <td
                        style='width:100%;text-align:center;padding-top:25px;padding-bottom:25px;'>
                        <p
                          style="margin: 0; color: #7a46e7; font-size: 18px; font-family: 'Google Sans', Roboto,RobotoDraft,Helvetica,Arial, sans serif; line-height: 120%; text-align: center; direction: ltr; letter-spacing: normal; margin-top: 0; margin-bottom: 0;">
                          Request for resetting your password
                        </p>
                      </td>
                    </tr>
                  </table>
                </th>
              </tr>
              </tbody>
            </table>
          </td>
        </tr>
        </tbody>
      </table>
      <table align='center' border='0' cellpadding='0' cellspacing='0' class='row row-3'
             role='presentation' style='mso-table-lspace: 0; mso-table-rspace: 0;' width='100%'>
        <tbody>
        <tr>
          <td>
            <table align='center' border='0' cellpadding='0' cellspacing='0'
                   class='row-content stack' role='presentation'
                   style='mso-table-lspace: 0; mso-table-rspace: 0;' width='500'>
              <tbody>
              <tr>
                <th class='column'
                    style='mso-table-lspace: 0; mso-table-rspace: 0; font-weight: 400; text-align: left; vertical-align: top;'
                    width='100%'>
                  <table border='0' cellpadding='0' cellspacing='0' class='text_block'
                         role='presentation'
                         style='mso-table-lspace: 0; mso-table-rspace: 0; word-break: break-word;'
                         width='100%'>
                    <tr>
                      <td
                        style='padding-top:15px;padding-right:10px;padding-bottom:10px;padding-left:10px;'>
                        <div style='font-family: Arial, sans-serif'>
                          <div
                            style="font-size: 12px; font-family: 'Google Sans', Roboto,RobotoDraft,Helvetica,Arial, sans-serif; color: #555555; line-height: 1.2;">
                            <p style='margin: 0; font-size: 12px;'><span
                              style='font-size:15px;'>Hi,</span></p>
                          </div>
                        </div>
                      </td>
                    </tr>
                  </table>
                  <table border='0' cellpadding='10' cellspacing='0'
                         class='text_block' role='presentation'
                         style='mso-table-lspace: 0; mso-table-rspace: 0; word-break: break-word;'
                         width='100%'>
                    <tr>
                      <td>
                        <div style='font-family: Arial, sans-serif'>
                          <div
                            style="font-size: 14px; color: #555555; line-height: 1.2; font-family: 'Google Sans', Roboto,RobotoDraft,Helvetica,Arial, sans-serif;">
                            <p style='margin: 0; font-size: 14px;'><span
                              style='font-size:15px;'>Someone (hopefully, you!) requested a password reset for your account. </span></p>
                          </div>
                        </div>
                      </td>
                    </tr>
                  </table>
                  <table border='0' cellpadding='10' cellspacing='0'
                         class='text_block' role='presentation'
                         style='mso-table-lspace: 0; mso-table-rspace: 0; word-break: break-word;'
                         width='100%'>
                    <tr>
                      <td>
                        <div style='font-family: Arial, sans-serif'>
                          <div
                            style="font-size: 14px; font-family: 'Google Sans', Roboto,RobotoDraft,Helvetica,Arial, sans-serif; color: #555555; line-height: 1.2;">
                            <p style='margin: 0; font-size: 14px;'><span
                              style='font-size:15px;'>Click the link below to choose a new password.</span>
                            </p>
                          </div>
                        </div>
                      </td>
                    </tr>
                  </table>
                  <table border='0' cellpadding='10' cellspacing='0'
                         class='divider_block' role='presentation'
                         style='mso-table-lspace: 0; mso-table-rspace: 0;' width='100%'>
                    <tr>
                      <td>
                        <div align='center'>
                          <table border='0' cellpadding='0' cellspacing='0'
                                 role='presentation'
                                 style='mso-table-lspace: 0; mso-table-rspace: 0;'
                                 width='100%'>
                            <tr>
                              <td class='divider_inner'
                                  style='font-size: 1px; line-height: 1px; border-top: 1px solid #e7e7e7;'>
                                <span></span>
                              </td>
                            </tr>
                          </table>
                        </div>
                      </td>
                    </tr>
                  </table>
                  <table border='0' cellpadding='10' cellspacing='0'
                         class='button_block' role='presentation'
                         style='mso-table-lspace: 0; mso-table-rspace: 0;' width='100%'>
                    <tr>
                      <td>
                        <div align='center'>
                          <!--[if mso]><a:roundrect xmlns:a="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" style="height:42px;width:190px;v-text-anchor:middle;" arcsize="10%" stroke="false" fillcolor="#3AAEE0"><w:anchorlock/><v:textbox inset="0px,0px,0px,0px"><center style="color:#ffffff; font-family:Arial, sans-serif; font-size:16px"><![endif]-->
                          <a
                            href="${resetLink}"
                            target='_blank'
                            style="text-decoration:none;display:inline-block;color:#ffffff;background-color:#7a46e7;border-radius:4px;width:auto;border: 1px solid #8454ea;padding-top:5px;padding-bottom:5px;font-family:'Google Sans', Roboto,RobotoDraft,Helvetica,Arial, sans-serif;text-align:center;mso-border-alt:none;word-break:keep-all;">
                            <span
                                      style='padding-left:20px;padding-right:20px;font-size:16px;display:inline-block;letter-spacing:normal;'><span
                                      style='font-size: 16px; line-height: 2; word-break: break-word; mso-line-height-alt: 32px;'>Reset your password</span></span>
                          </a>
                          <!--[if mso]></center></v:textbox></a:roundrect><![endif]-->
                        </div>
                      </td>
                    </tr>
                  </table>
                  <table border='0' cellpadding='0' cellspacing='0'
                         class='divider_block' role='presentation'
                         style='mso-table-lspace: 0; mso-table-rspace: 0;' width='100%'>
                    <tr>
                      <td
                        style='padding-top:10px;padding-right:10px;padding-bottom:15px;padding-left:10px;'>
                        <div align='center'>
                          <table border='0' cellpadding='0' cellspacing='0'
                                 role='presentation'
                                 style='mso-table-lspace: 0; mso-table-rspace: 0;'
                                 width='100%'>
                            <tr>
                              <td class='divider_inner'
                                  style='font-size: 1px; line-height: 1px; border-top: 1px solid #e7e7e7;'>
                                <span></span>
                              </td>
                            </tr>
                          </table>
                        </div>
                      </td>
                    </tr>
                  </table>
                </th>
              </tr>
              </tbody>
            </table>
          </td>
        </tr>
        </tbody>
      </table>
      <table align='center' border='0' cellpadding='0' cellspacing='0' class='row row-4'
             role='presentation' style='mso-table-lspace: 0; mso-table-rspace: 0;' width='100%'>
        <tbody>
        <tr>
          <td>
            <table align='center' border='0' cellpadding='0' cellspacing='0'
                   class='row-content stack' role='presentation'
                   style='mso-table-lspace: 0; mso-table-rspace: 0;' width='500'>
              <tbody>
              <tr>
                <th class='column'
                    style='mso-table-lspace: 0; mso-table-rspace: 0; font-weight: 400; text-align: left; vertical-align: top;'
                    width='100%'>
                  <table border='0' cellpadding='0' cellspacing='0' class='text_block'
                         role='presentation'
                         style='mso-table-lspace: 0; mso-table-rspace: 0; word-break: break-word;'
                         width='100%'>
                    <tr>
                      <td
                        style='padding-top:15px;padding-right:10px;padding-bottom:15px;padding-left:10px;'>
                        <div style='font-family: sans-serif'>
                          <div
                            style="font-size: 14px; color: #555555; line-height: 1.2; font-family: 'Google Sans', Roboto,RobotoDraft,Helvetica,Arial, sans-serif;">
                            <p style='margin: 0; font-size: 14px;'>If you
                              did not request a password reset, you can
                              simply ignore this message.</p>
                          </div>
                        </div>
                      </td>
                    </tr>
                  </table>
                </th>
              </tr>
              </tbody>
            </table>
          </td>
        </tr>
        </tbody>
      </table>
      <table align='center' border='0' cellpadding='0' cellspacing='0' class='row row-5'
             role='presentation' style='mso-table-lspace: 0; mso-table-rspace: 0;' width='100%'>
        <tbody>
        <tr>
          <td>
            <table align='center' border='0' cellpadding='0' cellspacing='0'
                   class='row-content stack' role='presentation'
                   style='mso-table-lspace: 0; mso-table-rspace: 0;' width='500'>
              <tbody>
              <tr>
                <th class='column'
                    style='mso-table-lspace: 0; mso-table-rspace: 0; font-weight: 400; text-align: left; vertical-align: top;'
                    width='100%'>
                  <table border='0' cellpadding='0' cellspacing='0' class='text_block'
                         role='presentation'
                         style='mso-table-lspace: 0; mso-table-rspace: 0; word-break: break-word;'
                         width='100%'>
                    <tr>
                      <td
                        style='padding-top:15px;padding-right:10px;padding-bottom:15px;padding-left:10px;'>
                        <div style='font-family: sans-serif'>
                          <div
                            style="font-size: 14px; color: #555555; line-height: 1.2; font-family: 'Google Sans', Roboto,RobotoDraft,Helvetica,Arial, sans-serif;">
                            <p style='margin: 0; font-size: 14px;'>
                              Regards,</p>
                            <p
                              style='margin: 0; margin-top: 5px; font-size: 14px; color: #8454ea'>
                              <strong>OakNut</strong>
                            </p>
                          </div>
                        </div>
                      </td>
                    </tr>
                  </table>
                </th>
              </tr>
              </tbody>
            </table>
          </td>
        </tr>
        </tbody>
      </table>
      <table align='center' border='0' cellpadding='0' cellspacing='0' class='row row-6'
             role='presentation' style='mso-table-lspace: 0; mso-table-rspace: 0;' width='100%'>
        <tbody>
        <tr>
          <td>
            <table align='center' border='0' cellpadding='0' cellspacing='0'
                   class='row-content stack' role='presentation'
                   style='mso-table-lspace: 0; mso-table-rspace: 0;' width='500'>
              <tbody>
              <tr>
                <th class='column'
                    style='mso-table-lspace: 0; mso-table-rspace: 0; font-weight: 400; text-align: left; vertical-align: top;'
                    width='100%'>
                  <table border='0' cellpadding='0' cellspacing='0'
                         class='heading_block' role='presentation'
                         style='mso-table-lspace: 0; mso-table-rspace: 0;' width='100%'>
                    <tr>
                      <td
                        style='width:100%;text-align:center;padding-top:25px;padding-bottom:25px;background-color: #e1e1e1;'>
                        <h1
                          style="margin: 0; color: #555555; font-size: 13px; font-family: 'Google Sans', Roboto,RobotoDraft,Helvetica,Arial, sans-serif; line-height: 120%; text-align: center; direction: ltr; font-weight: normal; letter-spacing: normal; margin-top: 0; margin-bottom: 0;">
                          <span>© 2021 OakNut. All rights reserved.</span>
                        </h1>
                      </td>
                    </tr>
                  </table>
                </th>
              </tr>
              </tbody>
            </table>
          </td>
        </tr>
        </tbody>
      </table>
    </td>
  </tr>
  </tbody>
</table><!-- End -->
</body>`;
};
