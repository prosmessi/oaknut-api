import { Response } from 'express';
import { JsonObject } from 'swagger-ui-express';
import { IApiResponse } from './interfaces';

/**
 *
 * @param obj
 * @param res
 * @description Sending the response in common API responsee
 */
export const sendResponseInJson = (
  obj: IApiResponse,
  res: Response,
): JsonObject => {
  const { ok, message, data } = obj;
  return res.json({
    ok,
    message,
    data,
  });
};
