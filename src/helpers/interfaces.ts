export interface IApiResponse {
  ok: boolean;
  message: string;
  data: object;
}

export interface IMailOptions {
  to: string;
  subject?: string;
  text?: string;
  html?: string;
}
