import nodemailer from 'nodemailer';
import { IMailOptions } from './interfaces';

const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'dndhawk.oaknut@gmail.com',
    pass:
      '#7!2HY2rRCo!WRq8xfhK&UiUghV5h$RjvprXidOWxvr$DCXGoTkzH73$8nbvM6*adGmMFF',
  },
});
export default function sendMail(mailOptions: IMailOptions): void {
  const mailData = {
    from: '"OakNut" <dndhawk.oaknut@gmail.com>',
    to: mailOptions.to,
    subject: mailOptions.subject || 'Oak Nut Reminder',
    html: mailOptions.html || '',
  };

  console.log(mailOptions);

  transporter.sendMail(mailData, function(error, info) {
    if (error) {
      console.log(error);
    } else {
      console.log('Email sent: ' + info.response);
    }
  });
}
