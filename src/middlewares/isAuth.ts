import * as express from 'express';
import jwt from 'jsonwebtoken';
import createError from 'http-errors';

const isAuth = (
  req: express.Request,
  res: express.Response,
  next: express.NextFunction,
): void => {
  const authHeader = req.get('Authorization');
  if (!authHeader) {
    return next(createError(401, 'Not authorized!'));
  }
  const token = authHeader.split(' ')[1];
  let decodedToken;
  try {
    decodedToken = jwt.verify(token, process.env.JWT_SECRET_KEY as string);
  } catch (e) {
    return next(createError(500, 'Token is either expired or not valid'));
  }
  if (!decodedToken) {
    return next(createError(401, 'Not authorized!'));
  }
  // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
  // @ts-ignore
  req.userId = decodedToken.userId;
  // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
  // @ts-ignore
  next();
};

export default isAuth;
